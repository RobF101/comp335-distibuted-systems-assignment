public class Job {
    private int subTime;
    private int id;
    private int estRunT;
    private int cpuReq;
    private int memReq;
    private int diskReq;

    public Job(int subTime, int id, int estRunT, int cpuReq, int memReq, int diskReq) {
        this.subTime = subTime;
        this.id = id;
        this.estRunT = estRunT;
        this.cpuReq = cpuReq;
        this.memReq = memReq;
        this.diskReq = diskReq;
    }

    public int getSubTime() {
        return subTime;
    }

    public void setSubTime(int subTime) {
        this.subTime = subTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEstRunT() {
        return estRunT;
    }

    public void setEstRunT(int estRunT) {
        this.estRunT = estRunT;
    }

    public int getCpuReq() {
        return cpuReq;
    }

    public void setCpuReq(int cpuReq) {
        this.cpuReq = cpuReq;
    }

    public int getMemReq() {
        return memReq;
    }

    public void setMemReq(int memReq) {
        this.memReq = memReq;
    }

    public int getDiskReq() {
        return diskReq;
    }

    public void setDiskReq(int diskReq) {
        this.diskReq = diskReq;
    }

    @Override
    public String toString() {
        return "Job{" +
                "subTime=" + subTime +
                ", id=" + id +
                ", estRunT=" + estRunT +
                ", cpuReq=" + cpuReq +
                ", memReq=" + memReq +
                ", diskReq=" + diskReq +
                '}';
    }
}
