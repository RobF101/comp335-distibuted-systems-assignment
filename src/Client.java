import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Collections;
import java.util.Comparator;
import java.util.Set;

public class Client {

    // initialize socket and input output streams
    private Socket socket = null;
    private DataInputStream input = null;
    private DataOutputStream out = null;
    private ArrayList<Job> jobList = new ArrayList<>(); //Unused client immediately sends job straight to server
    private LinkedHashMap<String, ServerType> serverTypes = new LinkedHashMap<>();
    private int clock;
    private String t;
    private String alg;
    private ArrayList<Server> initialServer = new ArrayList<>();
    private ArrayList<Server> serverList = new ArrayList<>();
    private int counter = 0;

    // constructor to put ip address and port
    public Client(String address, int port, String alg) {
        // establish a connection
        this.alg = alg;
        clock = 0;
        try {
            socket = new Socket(address, port);
            input = new DataInputStream(socket.getInputStream());// takes input from socket
            out = new DataOutputStream(socket.getOutputStream());// sends output to the socket
        } catch (UnknownHostException u) {
            System.out.println(u);
        } catch (IOException i) {
            System.out.println(i);
        }
    }

    public void start() {
        //Step 1 - Greeting wait for OK response
        send("HELO");
        if (!isOK()) {
            //
        }
        //Step 3 - Send Authentication
        send("AUTH rob");
        if (!isOK()) {
            //Somehow something went really wrong
            return;
        }
        //Collect server type information from XML file. must be run in server directory however.
        serverTypes = new XMLParser("system.xml").getServerTypes();

        //Step 5-11
        send("REDY");
        String job = inputBytesToString();
        while (!job.equals("NONE")) { //While jobs are available
            String[] jobSplit = job.split(" ");
            Job newJob = getNextJob(jobSplit);
            this.clock = newJob.getSubTime();

            //Dispatching
            findAndSendToServer(newJob);

            if (!isOK()) {
                //Somehow something went really wrong
                return;
            }
            //Request new job
            send("REDY");
            job = inputBytesToString();
        }
        //Step 12-14 close the connection
        disconnect();
    }

    //Input related
    private byte[] inputBytes() {
        ArrayList<Byte> msg = new ArrayList<Byte>();
        int z = 0;
        msg.add((byte) z);
        try {
            while (msg.get(msg.size() - 1) != (byte) ('\n')) {
                msg.add(input.readByte());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] arr = new byte[msg.size()];
        for (int i = 1; i < arr.length; i++) {
            arr[i] = msg.get(i);
            //System.out.print(arr[i]+",");
        }
        //System.out.println();
        //System.out.println("Recieved Raw: "+msg.toString());
        return arr;
    }

    private String inputBytesToString() {
        //Everything beyond this point needs changing...
        byte[] msg = inputBytes();
        String msgStr = "";
        for (int i = 0; i < msg.length; i++) {//Ignores new line character
            msgStr = msgStr + (char) ((byte) msg[i]);
        }
        //System.out.println("Recieved: "+msgStr);
        if (msgStr.equals(msgStr.trim())) {
            System.out.println("Last message: " + t);
            System.out.println("Recieved: " + msgStr);
        }
        //System.out.println(msgStr.equals(msgStr.trim()));
        t = msgStr;
        msgStr.trim();
        return msgStr.trim();
    }

    boolean isOK() {
        return inputBytesToString().equals("OK");
    }

    //Output Related
    private void send(String msg) {
        String toSend = msg + "\n";
        try {
            out.write(toSend.getBytes());
            //System.out.println("Sending: "+msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //Scheduling/Algorithm Related
    private void findAndSendToServer(Job j) {
        //Collect all server information
        getAllServerInfo();

        if (alg.equals("all2L")) {
            schedule(j, getBiggestServerType(), "0");
        } else if (alg.equals("ff")) {
            Server s = firstFit(j);
            schedule(j, s.getType(), s.getId().toString());
        } else if (alg.equals("bf")) {
            Server s = bestFit(j);
            schedule(j, s.getType(), s.getId().toString());
        } else if (alg.equals("wf")) {
            Server s = worstFit(j);
            schedule(j, s.getType(), s.getId().toString());
        }
    }

    private void schedule(Job j, String sT, String id) {
        send("SCHD " + j.getId().toString() + " " + sT + " " + id);
    }

    //Algorithms use serverType Hashmap's arrayList of servers (of that type) to search through the available servers
    // function to get init state
    private void saveInit(ArrayList<Server> s) {
        String check = "";
        for (Server ser : s) {
            if (!check.contains(ser.getType())) {
                check += ser.getType();
                initialServer.add(ser);
            }
        }
    }

    private Server getInitialServer(Job j) {
        for (Server ser : initialServer) {
            if (ser.checkCanDo(j)) {
                return ser;
            }
        }
        System.out.println("No server found");
        return null;
    }

    //For Someone to make.
    private Server firstFit(Job j) {
        // 1. Obtain server state information
        getAllServerInfo();

        // Sort serverTypes -> sortedServerTypes
        ArrayList<ServerType> sortedServerTypes = new ArrayList<ServerType>();
        for (String key : serverTypes.keySet()) {
            sortedServerTypes.add(serverTypes.get(key));
        }
        Collections.sort(sortedServerTypes, new Comparator<ServerType>() {
            @Override
            public int compare(ServerType s1, ServerType s2) {
                if (s1.getCoreC() < s2.getCoreC())
                    return -1;
                if (s1.getCoreC() > s2.getCoreC())
                    return 1;
                return 0;
            }
        });

        // 2. For each server type i, si , from the smallest to the largest
        for (ServerType sType : sortedServerTypes) {
            // 3. For each server j, si,j of server type si, from 0 to limit - 1 // j is server ID
            for (Server s : sType.getServers()) {
                // 4. If server si,j has sufficient available resources to run job ji then
                if (j.getMemReq() <= s.getMem() && j.getCpuReq() <= s.getCoreC() && j.getDiskReq() <= s.getDisk()) {
                    // 5. Return si,j
                    return s;
                }
            }
        }

        // 9. Return the first Active server with sufficient initial resource capacity to run job j
        for (String key : serverTypes.keySet()) {
            if (j.getMemReq() <= serverTypes.get(key).getMem() && j.getCpuReq() <= serverTypes.get(key).getCoreC() && j.getDiskReq() <= serverTypes.get(key).getDisk()) {//if the server type is good
                for (Server s : serverTypes.get(key).getServers()) {
                    if (s.getState() == 3) {
                        return s;
                    }
                }
            }
        }

        return null;
    }

    //Afrin made this in her branch
    private Server bestFit(Job j) {
        int bestFit = Integer.MAX_VALUE;
        Server bestServer = null;
        int minAvail = Integer.MAX_VALUE;
        //Server altServer = null;
        for (int i = 0; i < serverList.size(); i++) {
            if (serverList.get(i).checkCanDo(j)) {
                int fitVal = serverList.get(i).getCoreC() - j.getCpuReq();
                if (fitVal < bestFit || (fitVal == bestFit && serverList.get(i).getAvTime() < minAvail)) {
                    bestFit = fitVal;
                    bestServer = serverList.get(i);
                    minAvail = serverList.get(i).getAvTime();
                }
            }
        }
        if (bestServer != null) {
            bestFit = 9999;
            minAvail = 9999;
            return bestServer;
        } else {
            return getInitialServer(j);
        }

    }

    //For Someone to make.
    private Server worstFit(Job j) {
        int jM = j.getMemReq();
        int jCC = j.getCpuReq();
        int jD = j.getDiskReq();
        int worstFit = Integer.MIN_VALUE;
        int altFit = Integer.MIN_VALUE;
        Server wF = null;
        Server aF = null;

        for (String key : serverTypes.keySet()) {
            for (int i = 0; i < serverTypes.get(key).getServers().size(); i++) {
                Server s = serverTypes.get(key).getServers().get(i);
                int sM = s.getMem();
                int sCC = s.getCoreC();
                int sD = s.getDisk();
                if (jM <= sM && jCC <= sCC && jD <= sD) {
                    boolean actButAv = s.getState() == 3 && s.getAvTime() == -1; //Active but available servers
                    int fitVal = sCC - jCC;
                    if (fitVal > worstFit && (j.getSubTime() == s.getAvTime() || actButAv)) {//line 7 if fitVal < worstFit and server is immediately available
                        worstFit = fitVal;
                        wF = s;
                    } else if (fitVal > altFit && (s.getState() < 3 || actButAv)) {//if is available in a short definite amount of time
                        altFit = fitVal;
                        aF = s;
                    }
                }
            }
        }
        if (worstFit > Integer.MIN_VALUE) {
            return wF;
        } else if (altFit > Integer.MIN_VALUE) {
            return aF;
        }

        //if not found Return the worst-fit Active server based on initial resource capacity
        for (String key : serverTypes.keySet()) {
            int sM = serverTypes.get(key).getMem();
            int sCC = serverTypes.get(key).getCoreC();
            int sD = serverTypes.get(key).getDisk();
            if (jM <= sM && jCC <= sCC && jD <= sD) {//if the type of server is good
                int fitVal = sCC - jCC;
                for (int i = 0; i < serverTypes.get(key).getServers().size(); i++) {
                    Server s = serverTypes.get(key).getServers().get(i);
                    if (s.getState() == 3) {//if the server is active
                        if (fitVal > worstFit) {
                            worstFit = fitVal;
                            wF = s;
                        }
                    }
                }
            }
        }
        if (worstFit > Integer.MIN_VALUE) {
            return wF;
        }
        return null;
    }

    //Disconnection
    private void disconnect() {
        send("QUIT");
        if (inputBytesToString().equals("QUIT")) {
            try {
                input.close();
                out.close();
                socket.close();
            } catch (IOException i) {
                System.out.println(i);
            }
        }
    }

    //Job Operations
    private Job getNextJob(String[] s) {
        return new Job(
                Integer.parseInt(s[1]),
                Integer.parseInt(s[2]),
                Integer.parseInt(s[3]),
                Integer.parseInt(s[4]),
                Integer.parseInt(s[5]),
                Integer.parseInt(s[6])
        );
        //System.out.println(newJob.toString());
    }

    //ServerType operations
    private String getBiggestServerType() {
        int cpuC = 0;
        String ret = "";
        for (String key : serverTypes.keySet()) {
            if (serverTypes.get(key).getCoreC() >= cpuC) {
                cpuC = serverTypes.get(key).getCoreC();
                ret = key;
            }
        }
        return ret;
    }
    //RESC Related

    private void getAllServerInfo() {
        for (String key : serverTypes.keySet()) {
            serverTypes.get(key).getServers().clear();
        }
        serverList.clear();
        send("RESC All");
        if (!inputBytesToString().equals("DATA")) {
            //Issues...
        }
        send("OK");
        String server = "";
        server = inputBytesToString();
        ArrayList<Server> s = new ArrayList<>();
        while (!server.equals(".")) {
            String[] serverSplit = server.split(" ");
            serverTypes.get(serverSplit[0]).getServers().add(
                    new Server(serverSplit[0],
                            Integer.parseInt(serverSplit[1]),
                            Integer.parseInt(serverSplit[2]),
                            Integer.parseInt(serverSplit[3]),
                            Integer.parseInt(serverSplit[4]),
                            Integer.parseInt(serverSplit[5]),
                            Integer.parseInt(serverSplit[6])
                    ));

            //add to the new array list
            serverList.add(new Server(serverSplit[0],
                    Integer.parseInt(serverSplit[1]),
                    Integer.parseInt(serverSplit[2]),
                    Integer.parseInt(serverSplit[3]),
                    Integer.parseInt(serverSplit[4]),
                    Integer.parseInt(serverSplit[5]),
                    Integer.parseInt(serverSplit[6])));
            send("OK");
            server = inputBytesToString();
        }
        if (counter == 0) {
            saveInit(serverList);
            counter++;//idk why they didn't use a boolean for this but ok.
        }
    }

    //LSTJ Related (None Implemented so far)

    public static void main(String[] args) {
        if (args.length == 0) {
            new Client("127.0.0.1", 8096, "all2L").start();
        } else if (args.length > 2 || args.length == 1 || !(args[0].equals("-a") && (args[1].equals("ff") || args[1].equals("bf") || args[1].equals("wf")))) {
            System.out.println("Invalid format. Usage: \"java Client [-a [ff, bf, wf]]\" \n No parameters needed for allToLargest algorithm usage");
        } else {
            new Client("127.0.0.1", 8096, args[1]).start();
        }
    }
}