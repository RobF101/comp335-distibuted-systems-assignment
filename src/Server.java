public class Server {
    private String type;
    private Integer id;
    private int state;
    private int avTime;
    private int coreC;
    private int mem;
    private int disk;

    public Server(String type, int id, int state, int avTime, int coreC, int mem, int disk) {
        this.type = type;
        this.id = id;
        this.state = state;
        this.avTime = avTime;
        this.coreC = coreC;
        this.mem = mem;
        this.disk = disk;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getAvTime() {
        return avTime;
    }

    public void setAvTime(int avTime) {
        this.avTime = avTime;
    }

    public int getCoreC() {
        return coreC;
    }

    public void setCoreC(int coreC) {
        this.coreC = coreC;
    }

    public int getMem() {
        return mem;
    }

    public void setMem(int mem) {
        this.mem = mem;
    }

    public int getDisk() {
        return disk;
    }

    public void setDisk(int disk) {
        this.disk = disk;
    }

    public Boolean checkCanDo(Job j) {
        if (j.getCpuReq() <= this.getCoreC() && j.getMemReq() <= this.getMem() && j.getDiskReq() <= this.getDisk())
            return true;
        return false;
    }

    @Override
    public String toString() {
        return "Server{" +
                "type='" + type +
                ", id=" + id +
                ", state=" + state +
                ", avTime=" + avTime +
                ", coreC=" + coreC +
                ", mem=" + mem +
                ", disk=" + disk +
                '}';
    }
}