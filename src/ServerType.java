import java.util.ArrayList;

public class ServerType {
    private int lim;
    private int bootTime;
    private float hourlyRate;
    private int coreC;
    private int mem;
    private int disk;
    private ArrayList<Server> servers = new ArrayList<>();

    ServerType(int lim, int bootTime, float hourlyRate, int coreC, int mem, int disk) {
        this.lim = lim;
        this.bootTime = bootTime;
        this.hourlyRate = hourlyRate;
        this.coreC = coreC;
        this.mem = mem;
        this.disk = disk;
    }

    public int getLim() {
        return lim;
    }

    public void setLim(int lim) {
        this.lim = lim;
    }

    public int getBootTime() {
        return bootTime;
    }

    public void setBootTime(int bootTime) {
        this.bootTime = bootTime;
    }

    public float getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(float hourlyRate) {
        this.hourlyRate = hourlyRate;
    }

    public int getCoreC() {
        return coreC;
    }

    public void setCoreC(int coreC) {
        this.coreC = coreC;
    }

    public int getMem() {
        return mem;
    }

    public void setMem(int mem) {
        this.mem = mem;
    }

    public int getDisk() {
        return disk;
    }

    public void setDisk(int disk) {
        this.disk = disk;
    }

    public void setServers(ArrayList<Server> servers) {
        this.servers = servers;
    }

    public ArrayList<Server> getServers() {
        return servers;
    }

    @Override
    public String toString() {
        return "ServerType [lim=" + lim + ", bootTime=" + bootTime + ", hourlyRate=" + hourlyRate
                + ", coreC=" + coreC + ", mem=" + mem + ", disk=" + disk + "]";
    }

}