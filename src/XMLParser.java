import java.io.File;
import java.util.LinkedHashMap;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

public class XMLParser {

    private LinkedHashMap<String, ServerType> servers = new LinkedHashMap<>();

    XMLParser(String file) {
        try {
            //File xmlFile = new File("ds-sim/ds-config1.xml");
            File xmlFile = new File(file);
            DocumentBuilderFactory dbFac = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFac.newDocumentBuilder();
            Document doc = dBuilder.parse(xmlFile);
            doc.getDocumentElement().normalize();

            NodeList svrs = doc.getElementsByTagName("server");
            for (int i = 0; i < svrs.getLength(); i++) {
                Element svr = (Element) svrs.item(i);
                String t = svr.getAttribute("type");
                int l = Integer.parseInt(svr.getAttribute("limit"));
                int bt = Integer.parseInt(svr.getAttribute("bootupTime"));
                float hr = Float.parseFloat(svr.getAttribute("rate"));
                int cC = Integer.parseInt(svr.getAttribute("coreCount"));
                int m = Integer.parseInt(svr.getAttribute("memory"));
                int d = Integer.parseInt(svr.getAttribute("disk"));
                servers.put(t, new ServerType(l, bt, hr, cC, m, d));
                //System.out.println(t+ servers.get(t).toString());
            }

            //Debug Bit
            //System.out.println("Servers:");
            //for (int i = 0; i < servers.size(); i++) {
            //  System.out.println(servers.get(i).toString());
            //}
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public LinkedHashMap<String, ServerType> getServerTypes() {
        return servers;
    }

    public void setServerTypes(LinkedHashMap<String, ServerType> serverTypes) {
        this.servers = serverTypes;
    }
}
