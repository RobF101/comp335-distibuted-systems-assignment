Installation:
1.	Download this: https://bitbucket.org/RobF101/comp335-distibuted-systems-assignment/downloads/Installer.sh
2.	Open the terminal, naviagate to and Execute the Installer.sh file using bash Installer.sh
3.	If prompted type in sudo password. (This is to install java compilation and XML 
	libraries, check the installation script contents if you are worried).

How to run the client:
1.	Navigate to ds-ClientSource/comp335-distibuted-systems-assignment/ds-sim-06May2019/
2.	Run the server using ./ds-server -n -c CONFIG_NAME.xml
3.	Run the client using java Client.class or java Client.class -a [ff, bf, wf]